all:
	$(MAKE) -C debian/buster
	$(MAKE) -C debian/stretch
	$(MAKE) -C ubuntu/bionic

upload:
	rclone --copy-links --config ~/rclone.conf sync debian/ ovhswift:kwain_deb/debian/
	rclone --copy-links --config ~/rclone.conf sync ubuntu/ ovhswift:kwain_deb/ubuntu/

clean:
	rm -f $$(find . -name *Release*) $$(find . -name Packages*)
